#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <stdlib.h>
 
//Author: David Foret @ forez09@gmail.com
 
void signal_handler(int sig) {
    //No issues with signal-unsafe functions
    if(write(STDERR_FILENO, "GEN TERMINATED\n", 16)<0) {
        exit(2);   
    }
    exit(0);   
}
 
int main() {
    int fd[2];
    int exit_status = 0;
    pid_t GEN, NSD;
 
    if(pipe(fd) < 0) {
      return 2;  
    }
    //Sequence of pipe - fork - fork - kill - wait
    //FORK 1 - generator
    if((GEN = fork()) == 0) {
        //Only executed by gen
        close(fd[0]);
         
        if(dup2(fd[1], STDOUT_FILENO)<0) {
          close(fd[1]);
          return 2;    
        }
        close(fd[1]);
        signal(SIGTERM, signal_handler);
         
        while(1) {
          printf("%d %d\n", rand() % 4096, rand() % 4096);
          fflush(stdout);
          sleep(1);
        }
    } else {
        //Main process checks if GEN exists
        if(GEN < 0) {
            close(fd[0]);
            close(fd[1]);
            return 2;
        }
        //FORK 2 - NSD program
        if((NSD = fork()) == 0) {
            close(fd[1]);
             
            if(dup2(fd[0], STDIN_FILENO)<0) {
              close(fd[0]);
              return 2;    
            }
            close(fd[0]);
 
            if(execl("nsd", "", NULL) < 0) {
              return 2;
            }
        } else {
            //From here until end only executed by main process
            close(fd[0]);
            close(fd[1]);
 
            if(NSD < 0) {
              return 2;
            }
            sleep(5);
 
            if(GEN < 0) {
              return 2;
            }
            kill(GEN, SIGTERM);
            int child_status;
 
            for(int i=0; i < 2; i++) {
                wait(&child_status);
 
                if(child_status != 0) {
                    printf("ERROR\n");
                    exit_status = 1;
                }
            }
            exit_status ? exit(1) : printf("OK\n");;
        }    
    }  
}